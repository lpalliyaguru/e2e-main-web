<?php
include 'config.php';

$action = isset($_GET['action']) ? $_GET['action'] : null;

$connecting_string =  sprintf('mongodb://%s:%d/%s', DB_HOST, DB_PORT, DB_NAME);
$connection=  new Mongo($connecting_string,array('username' => DB_USER, 'password' => DB_PASS));

$db = $connection->selectDB(DB_NAME);
$collection = $db->emails;

if($action == 'email') {
    $email = $_GET['email'];
    $response = $collection->insert(array(
        'email' => $email,
        'info'  => $_SERVER,
        'time'  => new DateTime()
    ));

}
else if($action == 'survey') {
    sleep(2);
    $survey = getSurveyData();
    $db->survey->insert($survey);
}
echo json_encode(array('success' => true));



/* Functions */
function getSurveyData() {
    return array(
        'data' => $_POST['form'],
        'info' => $_SERVER
    );
}
